import pytimber
import numpy as np
import os, errno
from tqdm import tqdm as ProgressBar
import datetime as dt
import h5py
import time


# TODO Check for max fill number regardless of when it was created

class BBQTimberLogger:
	def __init__(self, fillNb=6500):
		self.__subDirLogPath = "data/"
		self.fillNb = fillNb
		# Last 2 elements in 2nd dimension are the start and end time of that beam mode
		self.beamModes = {}
		self.varNames = []
		self.ldb = pytimber.LoggingDB()
		# Sleep by this amount (in hours) if the fill being logged is ongoing
		self.__sleepyTime = 1

		# Initialize the member lists
		self.__updateBeamModeStartAndEndTimes()
		self.__initializeVarName()

	def __initializeVarName(self):
		self.varNames.append(
			self.ldb.tree.LHC.Beam_Instrumentation.Tune_and_Chroma.BBQ_Continuous_Gated.B1.get_vars())
		self.varNames.append(self.ldb.tree.LHC.Beam_Instrumentation.Tune_and_Chroma.BBQ_Continuous_Gated.B2
							 .get_vars())

	def __updateBeamModeStartAndEndTimes(self):
		# Initialize
		if len(self.beamModes) == 0:
			self.beamModes = {"INJPROT": [0, 0], "INJPHYS": [0, 0], "PRERAMP": [0, 0], "RAMP": [0, 0],
							  "FLATTOP": [0, 0], "SQUEEZE": [0, 0], "ADJUST": [0, 0], "STABLE": [0, 0]}

		# Update with current FillNb start and end times
		fillData = self.ldb.getLHCFillData(self.fillNb)
		for mode in fillData['beamModes']:
			if mode['mode'] in self.beamModes.keys():
				self.beamModes[mode['mode']][0] = mode['startTime']
				self.beamModes[mode['mode']][1] = mode['endTime']

	def __getData(self, varName, t1, t2, unixTime=True):
		if t2 < t1:
			print(varName)
			return None

		# Get raw data from variable
		dbRawData = self.ldb.get(varName, t1, t2, unixtime=unixTime)[varName]
		# print(dbRawData)
		# Store in dictionary and return it
		dbData = {}
		dbData['time'] = dbRawData[0]
		dbData['data'] = dbRawData[1]

		return dbData

	def __getFileName(self, beamNb, beamMode):
		# Create the filename and resolve its path on the machine
		script_dir = os.path.dirname(__file__)
		log_dir = os.path.join(script_dir, self.__subDirLogPath)

		if not os.path.exists(log_dir):
			try:
				os.makedirs(log_dir)
			except OSError as e:
				if e.errno != errno.EEXIST:
					raise

		fileName = log_dir + "Fill" + str(self.fillNb) + "_"
		fileName += "B" + str(beamNb) + "_" + beamMode

		return fileName

	def incFillNb(self):
		# check latest fill numbers in the last h hours
		h = 48

		# check fill nb limit here
		t2 = dt.datetime.now()
		t1 = t2 - dt.timedelta(hours=h)
		fills = []
		# only get fills which contain either one of out required beam modes
		for mode in self.beamModes:
			fills.append([fill['fillNumber'] for fill in self.ldb.getLHCFillsByTime(t1, t2, mode)])

		# Flatten the possibly imperfect multidimensional array
		fills = sum(fills, [])

		# If there were no fills in the last h hours
		if len(fills) == 0:
			print("No fills in the last " + str(h) + " hour{}, so fillNb will not be incremented.".format('s' if h !=
																												 1
																										  else ''))
			return False
		# If the last fill is not the current fill, update it
		elif max(fills) > self.fillNb:
			self.fillNb += 1
			self.__updateBeamModeStartAndEndTimes()
			# print(self.beamModes)
			return True

	def logFill(self, verbose = True):
		# Iterate from Beam 1 to Beam 2
		for beamNb in range(2):
			# Iterate through beamModes
			for beamModeKey in self.beamModes:
				fileName = self.__getFileName(beamNb + 1, beamModeKey)
				if verbose:
					print(fileName)
				data_file = h5py.File(fileName, 'w')

				buffer = []
				# Iterate through available variables in the current beam and beamMode
				for varName in self.varNames[beamNb]:
					# print(self.beamModes[beamModeKey])
					# Time will be None if fill in ongoing
					if not None in self.beamModes[beamModeKey]:
						t1 = self.beamModes[beamModeKey][0]
						t2 = self.beamModes[beamModeKey][1]
					else:
						# Sleep until ongoing fill with this beam_mode is over
						while None in self.beamModes[beamModeKey]:
							print("Fill {} is ongoing".format(self.fillNb))
							print("Logger will sleep for {} hour{} and then checks again".format(self.__sleepyTime,
																								 's' if self.__sleepyTime != 1 else ''))
							for i in ProgressBar(range(int(self.__sleepyTime * 3600))):
								time.sleep(1)
							self.__updateBeamModeStartAndEndTimes()
					if t1 == 0 or t2 == 0:
						pass
					else:
						buffer = self.__getData(varName, t1, t2)
						data_file.create_dataset(varName, data=np.asarray(buffer['data']))

				data_file.close()

	def getCurrentFillNb(self):
		return self.fillNb


#

if __name__ == "__main__":
	print("Starting logger at: {}".format(dt.datetime.now()))
	btl = BBQTimberLogger(6500)
	while True:
		print("Logging from Fill {}".format(btl.getCurrentFillNb()))
		btl.logFill()
		btl.incFillNb()
		print("\n\n")
